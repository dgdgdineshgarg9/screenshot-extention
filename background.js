let id = 0;
chrome.browserAction.onClicked.addListener(() => {
    
    /**
     * @param  {} (screenshotUrl
     * takes url and convert data URI format
     */
    chrome.tabs.captureVisibleTab((screenshotUrl) => {
        // generate new tab url
        const viewTabUrl = chrome.extension.getURL('app/screen.html?id=' + id++)
        let targetId = null;
    
    /**
     * @param  {viewTabUrl} //create new tab
     */
    chrome.tabs.create({url: viewTabUrl}, (tab) => {
      targetId = tab.id;
    });

    
    chrome.tabs.onUpdated.addListener(function listener(tabId,changedProps) {
        if (tabId != targetId || changedProps.status != "complete")
        return;
        chrome.tabs.onUpdated.removeListener(listener);
        const views = chrome.extension.getViews();
        for (let i = 0; i < views.length; i++) {
        let view = views[i];
        if (view.location.href == viewTabUrl) {
          view.setScreenshotImage(screenshotUrl);
          break;
        }
      }
    });
  });
});